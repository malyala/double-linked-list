/* 
Name: io.cpp
Author: Amit Malyala
Version: 0.1
License:
Copyright (c) <2017> <Amit Malyala>. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and 
the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Description: .
These functions provide functionality of a double linked list.

Notes:

Version and change history:
0.1 Initial version
0.2 Modified the file to read a list of 100000 dictionary words from a file.
*/

/*
Function Name : void ReadFileintoList(const std::string& Filename)
Description:
Read and display contents of a filename
From cplusplus.com.
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void ReadFileintoList(const std::string& Filename);

/*
Function Name : void ReadFileintoSortedList(const std::string& Filename)
Description:
Read and display contents of a filename
From cplusplus.com.
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void ReadFileintoSortedList(const std::string& Filename);

/*
Function Name : void WriteListintoFile(const std::string &Filename)
Description:
Write a doubly linked list into file.
From cplusplus.com.
Arguments: Input file name .
Preconditions: The function should write each element of a list as a line into a file.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found.
Known issues:
ReadFile function is not working properly on encrypted file on disk.
Unknown issues:
*/
void WriteListintoFile(const std::string &Filename);