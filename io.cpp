/* 
Name: io.cpp
Author: Amit Malyala
Version: 0.1
License:
Copyright (c) <2017> <Amit Malyala>. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and 
the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Description: 
These functions provide functionality of a double linked list.
Notes:
Version and change history:
0.1 Initial version
0.2 Modified the file to read strings from a file.
***********************************************************************/
#include <fstream>
#include <string>
#include <iostream>
#include "List.h"
#include "std_types.h"
/* Declare pHead */
extern List* pHead;
/* Declare Sentinel node */
extern List* pSentinel;
/* Variable for storing error status */
extern UINT32 Error;

/*
Function Name : void ReadFileintoList(const std::string& Filename)
Description:
Read and display contents of a filename
Arguments: Input file name 
Preconditions: The function should read a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found
*/
void ReadFileintoList(const std::string& Filename)
{
  std::string line;
  
  std::ifstream myfile (Filename);
  //SINT8 c;
  if (myfile.is_open())
  {
    //while (myfile.get(c ))  // For reading bytes
    while (getline(myfile,line ))
    {
       //std::cout << c ; // Printing bytes
      //std::cout << line << std::endl ; // Print a line
      //OriginalFile +=c;
      //OriginalFile +=line;
      if (line!="")
      {
      	//std::cout << "Inserting " << line << std::endl;
      	InsertNodeatEndofList(line);
	  }
      
    }
    myfile.close();
    
  }
  else
  {
      std::cout << std::endl << "File not found";
  }
}

/*
Function Name : void WriteListintoFile(const std::string &Filename)
Description:
Write a doubly linked list into file.
From cplusplus.com.
Arguments: Input file name .
Preconditions: The function should write each element of a list as a line into a file.If there are errors, an error message should be displayed.
Post conditions: Print error if file not found.
Known issues:
ReadFile function is not working properly on encrypted file on disk.
Unknown issues:
*/
void WriteListintoFile(const std::string &Filename)
{
  std::ofstream myfile (Filename);
  if (myfile.is_open())
  {
  	register List* pCurrent = pHead->pNext;
    if (pCurrent != pSentinel)
    {
     while(pCurrent != pSentinel)
      {
            myfile<< pCurrent->Word << "\n";
            pCurrent=pCurrent->pNext;
      }
    }
    else
    {
          Error = LISTEMPTY;
          ErrorMessage(Error);
    }
   	
    myfile.close();
  }
  else 
  {
     std::cout << "Unable to open file";
  }
}