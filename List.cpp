/*************************************************************************************************************************************
Name: List.cpp
Author: Amit Malyala
Version: 0.04
License:
SPDX license identifier: BSD 2-clause 
Copyright (c) <2017> <Amit Malyala>. All rights reserved.
Description: 
These functions provide functionality of a doubly linked list.

Notes:

To do:

Version and change history:
0.01 Initial version
0.02 Added sentinel node and changed functionality of pHead pointer
0.03 Added functionality for deleting all nodes in a list.
0.03 Added functionality for insrting nodes in a sorted list.
0.04 Added functionality for writing list into a file.

#include "List.h"
#include <string>
#include "std_types.h"
#include "iostream"
#include <ctype.h>
#include "io.h"

/* Declare pHead */
List* pHead =  nullptr;

/* Declare Sentinel node */
List* pSentinel = nullptr;

/* Variable for storing error status */
UINT32 Error = NOERROR;
/*
Function Name : List* GetNodeData(List* pNode)
Date : 25-05-2017
Description:
Gets data pertaining to a node.
Arguments: None
Pre conditions: The node passed onto the function should have memory allocated.
Post conditions: None
*/
List* GetNodeData(List* pNode)
{
    if(!(pNode))
    {
        Error = MEMALLOCERROR;
        return nullptr;
    }
   else
   {
    std::cout << "\nEnter a name: ";
    std::cin >> pNode->Word;
    return pNode;
   }
}

/*
Function Name : List* GetNodeData(List* pNode, const std::string& Word)
Date : 25-05-2017
Description:
Gets data pertaining to a node.
Arguments: None
Pre conditions: The node passed onto the function should have memory allocated.
Post conditions: None
*/
List* GetNodeData(List* pNode, const std::string& Word)
{
    if(!(pNode))
    {
        Error = MEMALLOCERROR;
        return nullptr;
    }
   else
   {
    pNode->Word = Word;
    return pNode;
   }
}
/*
Function Name : void InitList(void)
Date : 25-05-2017
Description:
After initialization, the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
Arguments: None
Pre conditions: There should not be memory allocation errors.
Post conditions: None
*/
void InitList(void)
{
	pHead = new List;
	pSentinel = new List;
	if ((pHead) && (pSentinel))
	{
		pHead->pPrevious=nullptr;
		pSentinel->pPrevious=pHead;
		pHead->pNext=pSentinel;
		pSentinel->pNext=nullptr;
	}
	else
    {
        Error = MEMALLOCERROR;
        ErrorMessage(Error);
    }
}

/*
Function Name : void InsertNodeatEndofList(void)
Date : 25-05-2017
Description:
The sentinel node is appended at the end of the list and each new node added
should replace the sentinel node with sentinel node being the next node of the 
new node added and the next node of sentinel node is nullptr
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr

With one element:
nullptr<-pHead<->pNode<->pSentinel->nullptr
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatEndofList(void)
{
    register List* pNode = new (List);
    pNode = GetNodeData(pNode);
    if(pNode)
    {
    /* When list is empty */
    if (pHead->pNext==pSentinel)
    {
        pNode->pNext= pSentinel;
        pSentinel->pNext=nullptr;
        pSentinel->pPrevious=pNode;
        pNode->pPrevious= pHead;
		pHead->pNext=pNode;
		pHead->pPrevious=nullptr;
    }
    else
    {
      List *pCurrent = pSentinel->pPrevious;
      pCurrent->pNext=pNode;
      pNode->pPrevious=pCurrent;
	        
	  pNode->pNext=pSentinel;
      pSentinel->pPrevious=pNode;
      pSentinel->pNext=nullptr;
    }
    }
    else
    {
        Error = MEMALLOCERROR;
        ErrorMessage(Error);
    }
}

/*
Function Name : void InsertNodeatEndofList(const std::string& Word)
Date : 25-05-2017
Description:
The sentinel node is appended at the end of the list and each new node added
should replace the sentinel node with sentinel node being the next node of the 
new node added and the next node of sentinel node is nullptr
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr

With one element:
nullptr<-pHead<->pNode<->pSentinel->nullptr
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatEndofList(const std::string& Word)
{
    register List* pNode = new (List);
    pNode = GetNodeData(pNode,Word);
    if(pNode)
    {
    /* When list is empty */
    if (pHead->pNext==pSentinel)
    {
        pNode->pNext= pSentinel;
        pSentinel->pNext=nullptr;
        pSentinel->pPrevious=pNode;
        pNode->pPrevious= pHead;
		pHead->pNext=pNode;
		pHead->pPrevious=nullptr;
    }
    else
    {
      List *pCurrent = pSentinel->pPrevious;
      pCurrent->pNext=pNode;
      pNode->pPrevious=pCurrent;
	        
	  pNode->pNext=pSentinel;
      pSentinel->pPrevious=pNode;
      pSentinel->pNext=nullptr;
    }
    }
    else
    {
        Error = MEMALLOCERROR;
        ErrorMessage(Error);
    }
}
/*
Function Name :  void InsertNodeintoSortedList(const std::string& Word)
Date : 25-05-2017
Description:
Inserts a node into a sorted list.
Notes:
Arguments: None
Pre conditions: List should not be empty. List should have nodes with word member in lowercase.
                List should be already sorted.
Post conditions: None
Known issues:
Unknown issues:
*/
void InsertNodeintoSortedList(const std::string& Word)
{
	
	register List *pNode= new List;
	register BOOL NodeInserted=false;
    pNode = GetNodeData(pNode,Word);
    /*
    std::cout << "Created Node" << std::endl;
    */
    /* list is empty */
    if (pHead->pNext == pSentinel)
    {
    	/*
    	std::cout << "List is empty" <<std::endl;
    	*/
    	pHead->pNext=pNode;
    	pNode->pPrevious=pHead;
    	pNode->pNext=pSentinel;
    	pSentinel->pPrevious=pNode;
    }
    else 
    {
       /*	 
       std::cout << "List has more than one or more element" << std::endl;	
       */
       register List* pCurrent = pHead->pNext;
       register List* pNextNode =  pCurrent->pNext;
       register List* pTemp = nullptr;
       /* Code edit here */
       while (pCurrent != pSentinel && NodeInserted==false)
       {
       	    if (pCurrent->Word <= pNode->Word  && pNextNode != pSentinel )
		  	{
		  		pCurrent=pCurrent->pNext;
		  		pNextNode=pNextNode->pNext;       	
			}
			else
			{
				/* Did not reach the end of list but incoming word is greater than a word element in list*/
				if (pNextNode != pSentinel)
				{
					/*
					std::cout << "Didn't reach end of list" << std::endl;
					std::cout << "pCurrent is greater than incoming word" << std::endl;
					*/
					/* PCurrent is greater than incoming word and we didnt reach end of list*/
					pTemp=pCurrent->pPrevious;
					pTemp->pNext=pNode;
					pNode->pPrevious=pTemp;
					pNode->pNext=pCurrent;
					pCurrent->pPrevious=pNode;
					NodeInserted=true;
				}
				
				else if (pNextNode == pSentinel)
				{
					/* We reach the end of list */
					/*
					std::cout << "Reached end of list " << std::endl;
					*/
					
					/* pCurrent is less than incoming word */
					if (pCurrent->Word <= pNode->Word)
					{
						/*
						std::cout << "pCurrent is less than incoming word" << std::endl;
						*/
						pCurrent->pNext=pNode;
						pNode->pPrevious=pCurrent;
						pCurrent=pNode; /* changed line */
						pCurrent->pNext=pNextNode;
						pNextNode->pPrevious=pCurrent;
						NodeInserted=true;
						
					}
					else if (pCurrent->Word > pNode->Word)
					{
						/*
						std::cout << "pCurrent is greater than incoming word" << std::endl;
						*/
						pTemp=pCurrent->pPrevious;
						pTemp->pNext=pNode;
						pNode->pPrevious=pTemp;
						pNode->pNext=pCurrent;
						pCurrent->pPrevious=pNode;
						NodeInserted=true;
					}
					
				} /* else if (pNextNode == pSentinel) */
			} /* else */
	   } /* while (pCurrent != pSentinel) */
	   /* Code edit end */
    } /* else */
}
/*
Function Name : void InsertNodeatBeginning(void)
Date : 25-05-2017
Description:
A node is added to the beginning of the list by moving the phead. 
The previous node of pHead would be nullptr
The next node of new node would be pHead->pNext.
The next node of pHead would be new node. 
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
With one element:
nullptr<-pHead<->pNode1<->pSentinel->nullptr
With more elements:
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatBeginning(void)
{
    register List* pNode = new  (List);
    pNode = GetNodeData(pNode);
    if(pNode)
    {
    /* When list is empty */
    if (pHead->pNext==pSentinel)
    {
        pNode->pNext= pSentinel;
        pSentinel->pNext=nullptr;
        pSentinel->pPrevious=pNode;
        pNode->pPrevious= pHead;
		pHead->pNext=pNode;
		pHead->pPrevious=nullptr;
    }
    else
    {
    	List *pTemp = pHead->pNext;
    	pNode->pNext=pTemp;
    	pHead->pNext=pNode;
    	pTemp->pPrevious=pNode;
    	pNode->pPrevious=pHead;
    }
    }
    else
    {
        Error = MEMALLOCERROR;
        ErrorMessage(Error);
    }
}
/*
Function Name :  void DisplayNodeForward(void)
Date : 25-05-2017
Description:
Displays Double linked list data in forward direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DisplayNodeForward(void)
{
    register List* pCurrent = pHead->pNext;
    if (pCurrent != pSentinel)
    {
     while(pCurrent != pSentinel)
      {
            std::cout << "\nWord is " <<pCurrent->Word;
            pCurrent=pCurrent->pNext;
      }
    }
    else
    {
          Error = LISTEMPTY;
          ErrorMessage(Error);
    }
}
/*
Function Name :  void DisplayNodeReverse(void)
Date : 25-05-2017
Description:
 Display Double linked list data in Reverse direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DisplayNodeReverse(void)
{
    register List* pCurrent = pSentinel->pPrevious;
    if (pCurrent != pHead)
    {
      while(pCurrent != pHead)
      {
        std::cout << "\nWord is " <<pCurrent->Word;
        pCurrent=pCurrent->pPrevious;
      }
    }
    else
    {
       Error = LISTEMPTY;
       ErrorMessage(Error);
    }

}
/*
Function Name : void DeleteAllNodes(void)
Date : 25-05-2017
Description:
Deletes Double linked list or tree data in forward direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DeleteAllNodes(void)
{
    if (pHead->pNext == pSentinel)
    {
       Error= LISTEMPTY;
       ErrorMessage(LISTEMPTY);
    }
    else if (pHead->pNext == pSentinel->pPrevious)
    {
 	   /* List has only one element. Delete that node in the list */
   	   List* pCurrent = pHead->pNext;
   	   delete pCurrent;
   	   pHead->pNext = pSentinel;
   	   pSentinel->pPrevious = pHead;
       
    }
    else 
    {
       List* pCurrent = pHead->pNext;
       List* pTemp =   nullptr;
       while (pCurrent != pSentinel)
       {
		  pTemp= pCurrent->pNext;
		  delete pCurrent;       	  	
		  pCurrent=pTemp;
	   }
	   pHead->pNext=pSentinel;
	   pSentinel->pPrevious=pHead;
    }
}
/*
Function Name :  void ReverseNodes(void)
Date : 25-05-2017
Description:
Reverses a double linked list.
While reversing a list all nodes between pHead and pSentinel are moved.
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
Reversing the list would give a empty list error.

With one element:
nullptr<-pHead<->pNode1<->pSentinel->nullptr
Reversing this list would keep the node intact.
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Reversing this list would interchange the nodes as 
nullptr<-pHead<->pNode2<->pNode1<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty or have a single node in which the function doesn't change the list.
Post conditions: None
*/
void ReverseNodes(void)
{
  register List *pCurrent= nullptr, *pNextNode= nullptr;
  pCurrent = pHead; 
  List* pTemp=pHead;
  if (pHead->pNext == pSentinel)
  {
 	Error= LISTEMPTY;
    ErrorMessage(Error);
  }
  else if (pHead->pNext== pSentinel->pPrevious)
  {
 	/* List has only one element. No need to reverse the list */
  }
  else 
  {
  	/*
 	With more elements:
    nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
    Reversing this list would interchange the nodes as 
    nullptr<-pHead<->pNode2<->pNode1<->pSentinel->nullptr

    nullptr<-pHead<->pNode1<->pNode2<-pNode3<->pSentinel->nullptr
    would become
    nullptr<-pHead<->pNode3<->pNode2<-pNode1<->pSentinel->nullptr
    */
   pHead =nullptr;
   while (pCurrent != nullptr)
   {
     pNextNode = pCurrent->pNext;
     pCurrent->pNext = pHead;
     pCurrent->pPrevious=pNextNode;
     pHead = pCurrent;
     pCurrent = pNextNode;
   }
   pSentinel=pTemp;
 }

}
/*
Function Name :  void DeleteNode(const std::string& SearchName)
Date : 25-05-2017
Description:
Delete nodes in a double linked List
Functionality for deleting memory allocated to be added 
Notes:
Design the function again.
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DeleteNode(const std::string& SearchName)
{
    register unsigned int Nodefound = false;
    if (pHead->pNext == pSentinel)
    {
       Error= LISTEMPTY;
       Nodefound = false;
    }
    else if (pHead->pNext== pSentinel->pPrevious)
    {
 	   /* List has only one element. */
 	   List* pCurrent = pHead->pNext;
 	   if (pCurrent->Word==SearchName)
 	   {
 	   	  Nodefound=true;	   	
 	   	  pHead->pNext=pSentinel;
 	   	  pSentinel->pPrevious=pHead;
 	   	  delete pCurrent;
	   }
	   else 
	   {
          Nodefound=false;
       }  
    }
    else 
    {
       List* pCurrent = pHead->pNext;
       List* pNextNode =  pCurrent->pNext;
       List* pTemp = nullptr;
       
       while (pCurrent != pSentinel)
       {
       	  if (pCurrent->Word==SearchName)
       	  {
       	  	 pTemp= pCurrent->pPrevious;
			 pNextNode->pPrevious=pTemp;
			 pTemp->pNext=pNextNode;
			 Nodefound=true;
			 delete pCurrent;       	  	
		  }
		  pCurrent=pCurrent->pNext;
		  pNextNode=pNextNode->pNext;
	   }
    }
    
    if (Nodefound == false)
    {
       Error = NODENOTFOUND;
       ErrorMessage(Error);
    }
}
/*
Function Name :  void DeleteNode(const std::string& SearchName)
Date : 25-05-2017
Description:
Function to detect cycle in double linked List 
Notes:
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/

unsigned int DetectCycleinList(void)
{
    List* pCurrent = pHead->pNext;
    List* pFast = pCurrent;
    unsigned int cycle = FALSE;
    if (pCurrent != pSentinel)
    {
    	while( (cycle==FALSE) && pCurrent->pNext != pSentinel)
       {
          if(!(pFast = pFast->pNext))
          {
            cycle= FALSE;
            break;
          }
          else if (pFast == pCurrent)
          {
            cycle = TRUE;
            break;
          }
          else if (!(pFast = pFast->pNext))
          {
            cycle = FALSE;
            break;
          }
          else if(pFast == pCurrent)
          {
             cycle = TRUE;
             break;
          }
          pCurrent=pCurrent->pNext;
        }
        if(cycle)
        {
          std::cout << "\nDouble Linked list/Tree is cyclic";
        }
        else
        {
    	  std::cout << "\nDouble Linked list/ Tree is not cyclic";
	    }
	}
	else
	{
		Error = LISTEMPTY;
		ErrorMessage(Error);
	}
    
    return cycle;
}
/* Function to display diagnostic errors */
void ErrorMessage(const int& Error)
 {
     switch(Error)
     {
         case LISTEMPTY:
         std::cout << "\nError: List is empty!";
         break;

         case MEMALLOCERROR:
         std::cout << "\nMemory allocation error ";
         break;

         case NODENOTFOUND:
         std::cout << "\nThe searched node is not found";
         break;

         default:
         std::cout << "\nError code missing\n";
         break;
     }
 }