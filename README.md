This project is a implementation of a double linked list in C++. A file containing a list of 100k sorted dictionary words 
is read into the list. Deletions, inserting of a string at the beginning or at the end or somewhere in 
the list are enabled. The double linked list can be used for other data types as well.

Author: Amit Malyala
Email: amit at malyala.net