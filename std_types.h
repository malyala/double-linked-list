
/***********************************************************
* Name: std_types.h                                        *
* Author: Amit Malyala                                     *
* Description: Custom data types for C/C++ programs        *
***********************************************************/
/*
License:
Copyright (c) <2017> <Amit Malyala>. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and 
the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef STD_TYPES_H
#define STD_TYPES_H

/*--------------------- Typedefs--------------------------*/
typedef bool BOOL;	   /* unsigned char 8 bits */
typedef int SINT32; 		   /* signed int 32 bits */
typedef unsigned int UINT32;   /* usnigned int 32 bits */
typedef unsigned short UINT16; /* unsigned short int 16 bits */
typedef unsigned long UINT64;  /* unsigned long */
typedef short SINT16; 		   /* signed short int 16 bits */
typedef unsigned char UINT8;   /* unsigned char 8 bits */
typedef char SINT8;    	       /* signed char 8 bits */
typedef double FLOAT64;		   /* Floating point double 64 bits */
typedef float  FLOAT32;		   /* Floating point float 32 bits */

typedef volatile int VSINT32; 		     /* signed int 32 bits */
typedef volatile unsigned int VUINT32;   /* usnigned int 32 bits */
typedef volatile unsigned short VUINT16; /* unsigned short int 16 bits */
typedef volatile short VSINT16; 		 /* signed short int 16 bits */
typedef volatile unsigned long VUINT64;  /* unsigned long */
typedef volatile unsigned char VUINT8;   /* unsigned char 8 bits */
typedef volatile char VSINT8;    	     /* signed char 8 bits */
typedef volatile double VFLOAT64;		 /* Floating point double 64 bits */
typedef volatile float  VFLOAT32;		 /* Floating point float 32 bits */

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#endif /* #ifndef STD_TYPES_H */