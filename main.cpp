/***************************************************
Name: main.cpp
Author: Amit
Description:  Implementation of a doubly linked list
in c++

Copyright (c) <2017> <Amit Malyala>. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided 
that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS I
NTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Change history:
0.1 Initial version
0.2 Modified the program to read list from a input file.
****************************************************/

#include "iostream"
#include "main.h"
#include "io.h"
#include "std_types.h"

SINT32 main(void)
{
    UINT32 choice =0;
    std::string InputName="";
    std::cout << "\nThis program creates a list";
    std::cout<< "\nYou can add nodes in forward and reverse directions";
    /* Initialize list
    After initialization, the list is as below: 
    nullptr<-pHead<->pSentinel->nullptr
    */
    InitList();
	do
    {
    	std::cout << std::endl;
        std::cout<< "\n1.Read list from file";
        std::cout<< "\n2.Insert Node at beginning of list";
        std::cout<< "\n3.Insert Node at end of list";
        std::cout<< "\n5.Insert Word into a sorted list";
        std::cout<< "\n6.Delete Node";
        std::cout<< "\n7.Display Nodes in forward direction";
        std::cout<< "\n8.Display Nodes in reverse direction";
        std::cout<< "\n9.Reverse nodes";
        std::cout<< "\n10.Detect cycle in list ";
        std::cout<< "\n11.Delete all nodes ";
        std::cout<< "\n12.Write List into file";
		std::cout<< "\n13.Exit\n";
        std::cout<< "\nEnter your choice: ";
        std::cin >> choice;
        switch(choice)
        {
     	      case 1:
        	  InputName="Wordsen.txt";
        	  ReadFileintoList(InputName);
        	  InputName="";
        	  break;
              case 2:
              InsertNodeatBeginning();
              break;
              case 3:
              InsertNodeatEndofList();
			  break;	
              case 4:
              std::cout <<"\nEnter the word you want to delete: ";
              std::cin >> InputName;
              DeleteNode(InputName);
              break;
              case 5:
              std::cout << "Inserting a node into a sorted list "<< std::endl;
              std::cout <<"\nEnter the word you want to insert: ";
              std::cin >> InputName;
              InsertNodeintoSortedList(InputName);
              InputName ="";
              break;
              case 6:
              std::cout <<"\nEnter the word you want to delete: ";
              std::cin >> InputName;
              DeleteNode(InputName);
			  break;	
            
              case 7:
              std::cout<< "\nDisplaying node data in forward direction \n";
              DisplayNodeForward();
              break;

              case 8:
              std::cout<< "\nDisplaying node data in reverse direction\n";
              DisplayNodeReverse();
              break;
            
              case 9:
              ReverseNodes();
              break;
                
              case 10:
              DetectCycleinList();
              break;
              case 11:
              std::cout<< "Deleting all nodes";
              DeleteAllNodes();
              break;
			  case 12:
              std::cout<< "Writing list into file Listoutput.txt";
              InputName="ListOutput.txt";
              WriteListintoFile(InputName);
              break;
              case 13:
              std::cout<< "Exiting program";
              break;
              default:
              std::cout<< "\nIncorrect choice\n";
              break;
       }

    } while (choice !=13);
    
    
    return 0;
}