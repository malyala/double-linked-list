/*****************************************************
Name: List.h
Author: Amit Malyala
Version: 0.3
License:
SPDX license identifier: BSD 2-clause 
Copyright (c) <2017> <Amit Malyala>. All rights reserved.

Description: 
These functions provide functionality a double linked list.
Change and revision history:
0.1 Initial version

Notes:
This program was implemented without a sentinel node by choosing a node as pHead and inserting nodes to the left or right.
Impelement the code without naked news or delete
From C++11:
"no naked news" ; that is, new belongs in constructors and similar operations, 
delete belongs in destructors, and together they provide a coherent memory management strategy. 


The double linked list will two type of nodes:
Admin nodes, normal nodes
This list would have two admin nodes, pHead and pSentinel which are always present in the list.
All other normal nodes which can vary in number would be added or deleted in between these two nodes and linked together 
to form a coherent list.

Design decision:
Should pHead point to the first node or first node is pHead->pNext
pHead=pFirstNode or pHead->pNextNode = pFirstNode.

To do:
Implement another version with pHead= pFirstNode.
For sorting list, use merge sort or std::sort for better performance.


Version and change history:
0.1 Initial version
0.2 Added sentinel node and changed functionality of pHead pointer
0.3 Added functionality for delting all nodes in a list.

******************************************************/
#ifndef DoubleLinkedList_H
#define DoubleLinkedList_H

#include "string"

/* Codes for errors */
enum Error_codes
{
 NOERROR=0x0,
 MEMALLOCERROR =0x1,
 LISTEMPTY =0x2,
 NODENOTFOUND =0x3
};

/* Double linked List definition */
typedef struct List
{
	/*
    char *Word ;
    */
    std::string Word;
    int count;
    struct List* pPrevious ;
    struct List* pNext ;
} List;

/*
Function Name : void InitList(void)
Date : 25-05-2017
Description:
After initialization:
The list is as below: 
nullptr<-pHead<->pSentinel->nullptr
Arguments: None
Pre conditions: There should not be memory allocation errors.
Post conditions: None
*/
void InitList(void);



/*
Function Name : List* GetNodeData(List* pNode)
Date : 25-05-2017
Description:
Gets data pertaining to a node.
Arguments: None
Pre conditions: The node passed onto the function should have memory allocated.
Post conditions: None
*/
List* GetNodeData(List* pNode);

/*
Function Name : List* GetNodeData(List* pNode, const std::string& Word)
Date : 25-05-2017
Description:
Gets data pertaining to a node.
Arguments: None
Pre conditions: The node passed onto the function should have memory allocated.
Post conditions: None
*/
List* GetNodeData(List* pNode, const std::string& Word);

/*
Function Name : void InsertNodeatEndofList(void)
Date : 25-05-2017
Description:
The sentinel node is appended at the end of the list and each new node added
should replace the sentinel node with sentinel node being the next node of the 
new node added and the next node of sentinel node is nullptr
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
With one element:
nullptr<-pHead<->pNode<->pSentinel->nullptr
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatEndofList(void);

/*
Function Name : void InsertNodeatEndofList(const std::string& Word)
Date : 25-05-2017
Description:
The sentinel node is appended at the end of the list and each new node added
should replace the sentinel node with sentinel node being the next node of the 
new node added and the next node of sentinel node is nullptr
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr

With one element:
nullptr<-pHead<->pNode<->pSentinel->nullptr
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatEndofList(const std::string& Word);

/*
Function Name :  void InsertNodeintoSortedList(const std::string& Word)
Date : 25-05-2017
Description:
Inserts a node into a sorted list
Notes:
Arguments: None
Pre conditions: List should not be empty.List should already be sorted.
Post conditions: None
*/
void InsertNodeintoSortedList(const std::string& Word);

/*
Function Name : void InsertNodeatBeginning(void)
Date : 25-05-2017
Description:
A node is added to the beginning of the list by moving the phead. 
The previous node of pHead would be nullptr
The next node of new node would be pHead->pNext.
The next node of pHead would be new node. 
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
With one element:
nullptr<-pHead<->pNode1<->pSentinel->nullptr
With more elements:
Arguments: None
Pre conditions: List should not be empty.There should not be any memory allocation errors.
Post conditions: None
*/
void InsertNodeatBeginning(void);

/*
Function Name :  void DisplayNodeForward(void)
Date : 25-05-2017
Description:
Displays Double linked list data in forward direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DisplayNodeForward(void);

/*
Function Name :  void DisplayNodeReverse(void)
Date : 25-05-2017
Description:
 Display Double linked list data in Reverse direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DisplayNodeReverse(void);


/*
Function Name :  void DeleteNode(const std::string& SearchName)
Date : 25-05-2017
Description:
Delete nodes in a double linked List
Functionality for deleting memory allocated to be added 
Notes:
Design the function again.
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DeleteNode(const std::string& name);

/*
Function Name :  void DeleteNode(const std::string& SearchName)
Date : 25-05-2017
Description:
Function to detect cycle in double linked List 
Notes:
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/

extern unsigned int DetectCycleinList(void);

/*
Function Name : void DeleteAllNodes(void)
Date : 25-05-2017
Description:
Deletes Double linked list or tree data in forward direction 
Arguments: None
Pre conditions: List should not be empty.
Post conditions: None
*/
void DeleteAllNodes(void);

/*
Function Name :  void ReverseNodes(void)
Date : 25-05-2017
Description:
Reverses a double linked list.
While reversing a list all nodes between pHead and pSentinel are moved.
After initialization,the list is as below: 
nullptr<-pHead<->pSentinel->nullptr
Reversing the list would give a empty list error.

With one element:
nullptr<-pHead<->pNode1<->pSentinel->nullptr
Reversing this list would keep the node intact.
With more elements:
nullptr<-pHead<->pNode1<->pNode2<->pSentinel->nullptr
Reversing this list would interchange the nodes as 
nullptr<-pHead<->pNode2<->pNode1<->pSentinel->nullptr
Arguments: None
Pre conditions: List should not be empty or have a single node in which the function doesn't change the list.
Post conditions: None
*/
void ReverseNodes(void);


/* function to display error message that List is empty */
void ErrorMessage(const int& Error);

#endif